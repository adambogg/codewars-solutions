# https://www.codewars.com/kata/55171d87236c880cea0004c6/train/python

from itertools import product

puzzle = [[9,0,0,0,8,0,0,0,1],
          [0,0,0,4,0,6,0,0,0],
          [0,0,5,0,7,0,3,0,0],
          [0,6,0,0,0,0,0,4,0],
          [4,0,1,0,6,0,5,0,8],
          [0,9,0,0,0,0,0,2,0],
          [0,0,7,0,3,0,2,0,0],
          [0,0,0,7,0,5,0,0,0],
          [1,0,0,0,4,0,0,0,7]]

puzzle2 = [[5,3,0,0,7,0,0,0,0],
           [6,0,0,1,9,5,0,0,0],
           [0,9,8,0,0,0,0,6,0],
           [8,0,0,0,6,0,0,0,3],
           [4,0,0,8,0,3,0,0,1],
           [7,0,0,0,2,0,0,0,6],
           [0,6,0,0,0,0,2,8,0],
           [0,0,0,4,1,9,0,0,5],
           [0,0,0,0,8,0,0,7,9]]

def is_illegal(grid):
    """
    Returns true if the grid has a repeated value on either axis or within a 3x3 cell group

    TODO this is wasted compute - we only need to check if the _new_ value breaks things, so can optimise by only checking its column, row, and 3x3 cell group

    TODO invert logic to is_legal?
    """

    # check each row
    for row in grid:
        row = [d for d in row if d != 0]
        if len(row) > len(set(row)):
            return True

    # check each column
    for colIndex in range(0, len(grid)):
        col = [i[colIndex] for i in grid]
        col = [d for d in col if d != 0]
        if len(col) > len(set(col)):
            return True

    # check each 3x3
    for rowIndex in range(0, 3):
        rowIndices = range(rowIndex * 3, (rowIndex * 3) + 3)
        for colIndex in range(0, 3):
            colIndices = range(colIndex * 3, (colIndex * 3) + 3)
            cells = product(rowIndices, colIndices)

            values = [grid[row][col] for [row, col] in cells]
            values = [d for d in values if d != 0]

            if len(values) > len(set(values)):
                return True
    
    # check for 10s
    for row in grid:
        for cell in row:
            if cell > 9:
                return True

    return False

assert is_illegal(puzzle2) == False
test_puzzle = [row[:] for row in puzzle2]
test_puzzle[0][2] = 3
assert is_illegal(test_puzzle) == True
test_puzzle = [row[:] for row in puzzle2]
test_puzzle[1][1] = 3
assert is_illegal(test_puzzle) == True
test_puzzle = [row[:] for row in puzzle2]
test_puzzle[7][6] = 9
assert is_illegal(test_puzzle) == True

def solve(grid):
    """
    Determine all empty cells, iterate through with 'backtracking' method:
    Set empty cell to 1, if valid, move on, else increment. If 9 isn't valid, move backwards and increment
    Repeat until 'solve' found - not checking for impossible sudoku
    """

    cells_missing_values = [] # [(rowIndex, colIndex)...]

    for rowIndex in range(0, len(grid)):
        for colIndex in range(0, len(grid)):
            if grid[rowIndex][colIndex] == 0:
                cells_missing_values.append((rowIndex, colIndex))

    attempted_values = [0]

    def solved():
        return len(cells_missing_values) + 1 == len(attempted_values) and not is_illegal(grid)

    index = 0
    while not solved():
        [rowIndex, colIndex] = cells_missing_values[index]
        attempted_values[-1] += 1
        grid[rowIndex][colIndex] = attempted_values[-1]

        if is_illegal(grid):
            if attempted_values[-1] < 9:
                continue # same index, try a larger number
            else:
                attempted_values = attempted_values[:-1]
                grid[rowIndex][colIndex] = 0
                index -= 1
        else:
            attempted_values.append(0)
            index += 1 # TODO can index just be len(attempted_values) - 1?

    return grid

def is_solved(grid):
    if is_illegal(grid):
        return False
    
    # check each row
    for row in grid:
        row = [d for d in row if d != 0]
        if len(set(row)) != 9:
            return False

    # check each column
    for colIndex in range(0, len(grid)):
        col = [i[colIndex] for i in grid]
        col = [d for d in col if d != 0]
        if len(set(col)) != 9:
            return False

    # check each 3x3
    for rowIndex in range(0, 3):
        rowIndices = range(rowIndex * 3, (rowIndex * 3) + 3)
        for colIndex in range(0, 3):
            colIndices = range(colIndex * 3, (colIndex * 3) + 3)
            cells = product(rowIndices, colIndices)

            values = [grid[row][col] for [row, col] in cells]
            values = [d for d in values if d != 0]

            if len(set(values)) != 9:
                return False
    
    return True

assert is_solved(solve(puzzle)) == True
assert is_solved(solve(puzzle2)) == True