# https://www.codewars.com/kata/56b2abae51646a143400001d/train/python

t_one = ['0', '1']

def t(n):
    if n == 1:
        return t_one

    table = t(n - 1)

    res = [f"0{i}" for i in table] + [f"1{i}" for i in table[::-1]]
    return res

def mystery(n):
    number_of_bits_in_n = len(bin(n)) - 2
    table = t(number_of_bits_in_n)
    return int(table[n], 2)

def test_mystery(n):
    b = list(bin(n)[2:])
    b[1] = "0" if b[1] == "1" else "1"
    if len(b) % 2 != 0:
        b[-1] = "0" if b[-1] == "1" else "1"
    return int("".join(b), 2)

def mystery_inv(n):
    pass

def name_of_mystery():
    return "foo"

mystery(2)
assert mystery(6) == 5
assert mystery(9) == 13
assert mystery(19) == 26
assert mystery_inv(5) == 6
assert mystery_inv(13) == 9
assert mystery_inv(26) == 19