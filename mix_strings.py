# https://www.codewars.com/kata/5629db57620258aa9d000014/train/python

import re
from collections import defaultdict

def mix(s1, s2):
    s1 = re.sub("[^a-z]+", "", s1)
    s2 = re.sub("[^a-z]+", "", s2)

    letters = set(s1 + s2)
    freq = defaultdict(dict)
    counts = set()
    for letter in letters:
        count_one = s1.count(letter)
        count_two = s2.count(letter)

        if max(count_one, count_two) > 1:
            count = count_one if count_one > count_two else count_two
            counts.add(count)
            if count_one == count_two:
                freq[count_one][letter] = "="
            elif count_one > count_two:
                freq[count_one][letter] = "1"
            elif count_two > count_one:
                freq[count_two][letter] = "2"

    counts = [i for i in counts]
    counts.sort()
    res = []

    for count in counts[::-1]:
        val = freq[count]
        val_res = [f"{val[key]}:{key * count}" for key in val]
        val_res.sort()
        res.extend(val_res)

    return "/".join(res)


mix("Are they here", "yes, they are here") == "2:eeeee/2:yy/=:hh/=:rr"
mix("looping is fun but dangerous", "less dangerous than coding") == "1:ooo/1:uuu/2:sss/=:nnn/1:ii/2:aa/2:dd/2:ee/=:gg"
mix(" In many languages", " there's a pair of functions") == "1:aaa/1:nnn/1:gg/2:ee/2:ff/2:ii/2:oo/2:rr/2:ss/2:tt"
mix("Lords of the Fallen", "gamekult") == "1:ee/1:ll/1:oo"
mix("codewars", "codewars") == ""
mix("A generation must confront the looming ", "codewarrs") == "1:nnnnn/1:ooooo/1:tttt/1:eee/1:gg/1:ii/1:mm/=:rr"