# https://www.codewars.com/kata/59de1e2fe50813a046000124/train/python
import re

def change(s, prog, ver):
    phone = re.findall(r"Phone: (\+1(?:-\d{3}){2}-\d{4})", s)
    version = re.findall(r"Version: (\d+\.\d+)(?!\.\d)", s)

    if phone == [] or version == []:
        return "ERROR: VERSION or PHONE"

    print_ver = ver if version[0] != "2.0" else "2.0"

    # Program Author Phone Date Version
    return f"Program: {prog} Author: g964 Phone: +1-503-555-0090 Date: 2019-01-01 Version: {print_ver}"

s1 = 'Program title: Primes\nAuthor: Kern\nCorporation: Gold\nPhone: +1-503-555-0091\nDate: Tues April 9, 2005\nVersion: 6.7\nLevel: Alpha'
s2 = 'Program title: Hammer\nAuthor: Tolkien\nCorporation: IB\nPhone: +1-503-555-0090\nDate: Tues March 29, 2017\nVersion: 2.0\nLevel: Release'
s3 = 'Program title: Primes\nAuthor: Kern\nCorporation: Gold\nPhone: +1-503-555-009\nDate: Tues April 9, 2005\nVersion: 6.7\nLevel: Alpha'

assert change(s1, 'Ladder', '1.1') == 'Program: Ladder Author: g964 Phone: +1-503-555-0090 Date: 2019-01-01 Version: 1.1'
assert change(s2, 'Balance', '1.5.6') == 'Program: Balance Author: g964 Phone: +1-503-555-0090 Date: 2019-01-01 Version: 2.0'
assert change(s3, 'Ladder', '1.1') == 'ERROR: VERSION or PHONE'