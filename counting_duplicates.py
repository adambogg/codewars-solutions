# https://www.codewars.com/kata/54bf1c2cd5b56cc47f0007a1/train/python

def duplicate_count(text):
    text = text.lower()
    multi = set()
    
    for index, char in enumerate(text):
        if char in text[0:index]:
            multi.add(char)
    
    return len(multi)

assert duplicate_count("") == 0
assert duplicate_count("abcde") == 0
assert duplicate_count("abcdeaa") == 1
assert duplicate_count("abcdeaB") == 2
assert duplicate_count("Indivisibilities") == 2
assert duplicate_count("I6BkmEBnJhWjUp3yf8vSX5Tx58L2zmqdsqNyR2TTpXmSwViJESJiNjRoKi0qA19QlYbIXYYS6NHNHlbaiHQFa") == 24