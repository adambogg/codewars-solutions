# https://www.codewars.com/kata/569d488d61b812a0f7000015/train/python

def data_reverse(data):
    new = []
    for i in range(0, len(data), 8):
        new.append(data[i:i + 8])

    new.reverse()
    return [bit for segment in new for bit in segment]

data1 = [1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,1,0,1,0,1,0]
data2 = [1,0,1,0,1,0,1,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1]
assert data_reverse(data1) == data2