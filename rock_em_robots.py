# https://www.codewars.com/kata/566b490c8b164e03f8000002/train/python

def __attack__(robot_1, robot_2, tactics): # robot 1 attacks robot 2
    if robot_1["tactics"] != []:
        robot_2["health"] -= tactics[robot_1["tactics"][0]]
        robot_1["tactics"] = robot_1["tactics"][1:]

    return (robot_1, robot_2)

def fight(robot_1, robot_2, tactics):
    first = robot_2 if robot_1["speed"] < robot_2["speed"] else robot_1
    second = robot_1 if first == robot_2 else robot_2
    do_fight = first["health"] > 0 and second["health"] > 0 and (first["tactics"] != [] or second["tactics"] != [])
    
    while do_fight:
        first, second = __attack__(first, second, tactics)
        if second["health"] > 0:
            second, first = __attack__(second, first, tactics)
        do_fight = first["health"] > 0 and second["health"] > 0 and first["tactics"] != [] and second["tactics"] != []
    
    if first["health"] < 0 and second["health"] < 0:
        return "The fight was a draw."
    if first["health"] < 0:
        return second["name"] + " has won the fight."
    if second["health"] < 0:
        return first["name"] + " has won the fight."