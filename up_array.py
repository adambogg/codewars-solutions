def up_array(arr):
    if len(arr) != len([i for i in arr if i >= 0]):
        return None
    return [int(i) for i in str((int("".join(str(i) for i in arr)) + 1))]

assert up_array([2,3,9]) == [2,4,0]
assert up_array([4,3,2,5]) == [4,3,2,6]
assert up_array([1,-9]) == None