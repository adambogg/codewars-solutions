eats = {
    "antelope": ["grass"],
    "big-fish": ["little-fish"],
    "bug": ["leaves"],
    "bear": ["big-fish", "bug", "chicken", "cow", "leaves", "sheep"],
    "chicken": ["bug"],
    "cow": ["grass"],
    "fox": ["chicken", "sheep"],
    "giraffe": ["leaves"],
    "lion": ["antelope", "cow"],
    "panda": ["leaves"],
    "sheep": ["grass"]
}

def __bite__(zoo):
    # do one eat
    prey = None
    action = None
    index = 0

    while prey == None and index < len(zoo):
        animal = zoo[index]
        
        if animal in eats:
            if index != 0: # test left if current animal has an animal to its left
                if zoo[index - 1] in eats[animal]:
                    prey = index - 1
                    break
            if index != len(zoo) - 1: # test right if current animal has an animal to its right
                if zoo[index + 1] in eats[animal]:
                    prey = index + 1
                    break
        
        index += 1

    if prey != None:
        predator = animal
        action = f"{predator} eats {zoo[prey]}"
        del zoo[prey]
    
    return (action, zoo)

def who_eats_who(zoo):
    result = [zoo[:]]
    zoo = zoo.split(",")

    # add each step to result
    current = __bite__(zoo)
    while current[0] != None:
        result.append(current[0])
        zoo = current[1]
        current = __bite__(zoo)

    result.append(",".join(zoo))
    print(result)
    return result

test_input = "fox,bug,chicken,grass,sheep"
expected = ["fox,bug,chicken,grass,sheep", 
            "chicken eats bug", 
            "fox eats chicken", 
            "sheep eats grass", 
            "fox eats sheep", 
            "fox"]
assert who_eats_who(test_input) == expected