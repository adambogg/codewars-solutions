import re

def count_smileys(arr):
    return len([i for i in arr if re.match(r"[:;][-~]?[)D]", i)])

assert count_smileys([]) == 0
assert count_smileys([':D',':~)',';~D',':)']) == 4
assert count_smileys([':)',':(',':D',':O',':;']) == 2
assert count_smileys([';]', ':[', ';*', ':$', ';-D']) == 1