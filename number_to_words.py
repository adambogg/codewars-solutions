import re

digitWords = {
    "0": "zero",
    "1": "one",
    "2": "two",
    "3": "three",
    "4": "four",
    "5": "five",
    "6": "six",
    "7": "seven",
    "8": "eight",
    "9": "nine",
    "10": "ten",
    "11": "eleven",
    "12": "twelve",
    "13": "thirteen",
    "14": "fourteen",
    "15": "fifteen",
    "16": "sixteen",
    "17": "seventeen",
    "18": "eighteen",
    "19": "nineteen",
    "20": "twenty",
    "30": "thiry",
    "40": "forty",
    "50": "fifty",
    "60": "sixty",
    "70": "seventy",
    "80": "eighty",
    "90": "ninety"
}

suffixes = {
    0: "",
    1: "",
    2: "hundred",
    3: "thousand",
    4: "thousand",
    5: "thousand",
    6: "million",
    7: "million"
}

def __strip_leading_zeroes__(s):
    return re.match(r"^(?:0*)(\d*)", s)[1]

def n2w(s):
    # generate word (from one or two digits)
    word = s[:2] if len(s) % 2 == 0 else s[0]

    # generate phrase
    if word in digitWords:
        phrase = f"{digitWords[word]}" 
    else:
        word_one = f"{word[0]}0"
        word_two = word[1]
        phrase = f"{digitWords[word_one]}-{digitWords[word_two]}"

    remaining = s[len(word):]
    suffix = suffixes[len(remaining)]

    if suffix != "":
        phrase += f" {suffix}"

    remaining = __strip_leading_zeroes__(remaining)

    if remaining == "":
        return phrase

    return f"{phrase} {n2w(remaining)}"

def number_to_words(n):
    d = { 0 : 'zero', 1 : 'one', 2 : 'two', 3 : 'three', 4 : 'four', 5 : 'five',
          6 : 'six', 7 : 'seven', 8 : 'eight', 9 : 'nine', 10 : 'ten',
          11 : 'eleven', 12 : 'twelve', 13 : 'thirteen', 14 : 'fourteen',
          15 : 'fifteen', 16 : 'sixteen', 17 : 'seventeen', 18 : 'eighteen',
          19 : 'nineteen', 20 : 'twenty',
          30 : 'thirty', 40 : 'forty', 50 : 'fifty', 60 : 'sixty',
          70 : 'seventy', 80 : 'eighty', 90 : 'ninety' }
    k = 1000
    m = k * 1000
    
    assert(n >= 0)

    if (n < 20):
        return d[n]

    if (n < 100):
        if n % 10 == 0: return d[n]
        else: return d[n // 10 * 10] + '-' + d[n % 10]

    if (n < k):
        if n % 100 == 0: return d[n // 100] + ' hundred'
        else: return d[n // 100] + ' hundred ' + number_to_words(n % 100)

    if (n < m):
        if n % k == 0: return number_to_words(n // k) + ' thousand'
        else: return number_to_words(n // k) + ' thousand ' + number_to_words(n % k)

def number2words(n):
    """ works for integers between 0 and 999999 """
    return number_to_words(n)

assert number2words(0) == "zero"
assert number2words(1) == "one"
assert number2words(8) == "eight"
assert number2words(10) == "ten"
assert number2words(19) == "nineteen"
assert number2words(20) == "twenty"
assert number2words(22) == "twenty-two"
assert number2words(54) == "fifty-four"
assert number2words(80) == "eighty"
assert number2words(98) == "ninety-eight"
assert number2words(100) == "one hundred"
assert number2words(301) == "three hundred one"
assert number2words(793) == "seven hundred ninety-three"
assert number2words(800) == "eight hundred"
assert number2words(650) == "six hundred fifty"
assert number2words(1000) == "one thousand"
assert number2words(1003) == "one thousand three"