def is_valid_IP(string):
    if " " in string:
        return False
    
    parts = [i for i in string.split(".") if len(i) > 0]
    
    if len(parts) != 4:
        return False
    
    alpha_parts = [i for i in parts if not i.isdigit()]
    if len(alpha_parts) > 0:
        return False
    
    leading_zeroes = [i for i in parts if i[0] == '0' and len(i) > 1]
    if len(leading_zeroes) > 0:
        return False
    
    parts = [int(i) for i in parts]
    good_parts = [i for i in parts if 0 <= i <= 255]
    
    return len(good_parts) == 4

assert is_valid_IP('169.117.123.00') == False
assert is_valid_IP('127.1.1.0') == True
assert is_valid_IP('0.0.0.0') == True
assert is_valid_IP('0.34.82.53') == True
