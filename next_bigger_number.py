# https://www.codewars.com/kata/55983863da40caa2c900004e/train/python

def next_bigger(n):
    chars = sorted(list(str(n)))
    chars.reverse()
    if n == int("".join(chars)):
        return -1
    
    original = sorted(list(str(n)))
    
    while True:
        n += 1
        if sorted(list(str(n))) == original:
            return n

next_bigger(12) == 21
next_bigger(513) == 531
next_bigger(2017) == 2071
next_bigger(414) == 441
next_bigger(144) == 414
next_bigger(123456789) == 123456798
next_bigger(1234567890) == 1234567908
next_bigger(9876543210) == -1
next_bigger(9999999999) == -1
next_bigger(59884848459853) == 59884848483559