import random
from functools import reduce

def foo(var):
    return random.randint(1, 10) * var

arr = [random.randint(1, 10) for _ in range(10)]
print(arr)

mapped = map(foo, arr)
print(mapped)

def bar(var, var2):
    return f"{var} : {var2}"

reduced = reduce(bar, mapped)
print(reduced)