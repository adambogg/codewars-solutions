# https://www.codewars.com/kata/51e0007c1f9378fa810002a9/train/python

def parse(data):
    pond = []
    
    val = 0
    for instr in data:
        match instr:
            case 'i':
                val += 1
            case 'd':
                val -= 1
            case 's':
                val = val ** 2
            case 'o':
                pond.append(val)
    
    return pond

assert parse("ooo") == [0,0,0]
assert parse("ioioio") == [1,2,3]
assert parse("idoiido") == [0,1]
assert parse("isoisoiso") == [1,4,25]
assert parse("codewars") == [0]
