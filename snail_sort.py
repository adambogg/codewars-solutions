# https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1/train/python

def snail(snail_map):
    x, y = 0, 0
    dx, dy = 1, 0

    res = []

    seen = []
    count = sum([len(i) for i in snail_map])

    while len(res) < count:
        res.append(snail_map[y][x])
        seen.append((x, y))
        
        if dx == 1 and ((x + 1, y) in seen or x + 1 == len(snail_map[0])):
            dx, dy = 0, 1
        elif dx == -1 and ((x - 1, y) in seen or x - 1 == -1):
            dx, dy = 0, -1
        elif dy == 1 and ((x, y + 1) in seen or y + 1 == len(snail_map)):
            dx, dy = -1, 0
        elif dy == -1 and ((x, y - 1) in seen or y - 1 == -1):
            dx, dy = 1, 0
        
        x += dx
        y += dy

    return res

array = [[1,2,3],
         [4,5,6],
         [7,8,9]]
expected = [1,2,3,6,9,8,7,4,5]
assert snail(array) == expected


array = [[1,2,3],
         [8,9,4],
         [7,6,5]]
expected = [1,2,3,4,5,6,7,8,9]
assert snail(array) == expected
