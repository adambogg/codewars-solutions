import re

# PROBLEM: current solution is number of bombs in immediate neighbours
# solution needs to be number of bombs between start/previous shelter and next shelter/end

def alphabet_war(battlefield):
    temp = [i for i in re.findall(r"(#+)|([a-z]+)|(\[[a-z]+])", battlefield)]
    split = []
    for i in temp:
        for j in i:
            if j != "":
                split.append(j)
    survivors = []
    for i, group in enumerate(split):
        prev_group = split[i - 1] if i > 0 else ""
        next_group = split[i + 1] if i < len(split) - 1 else ""
        around_group = prev_group + next_group
        if group.startswith("[") and group.endswith("]"): # in bunker
            if "##" not in around_group:
                survivors.extend(re.findall(r"([a-z]+)", group))
        else:
            if battlefield.count("#") == 0:
                survivors.extend(group)
    print(survivors)
    return "".join(survivors)

assert alphabet_war('[a]#[b]#[c]') == 'ac'
assert alphabet_war('[a]#b#[c][d]') =='d'
assert alphabet_war('[a][b][c]') == 'abc'
assert alphabet_war('##a[a]b[c]#') =='c'
assert alphabet_war('abde[fgh]ijk') == 'abdefghijk'
assert alphabet_war('ab#de[fgh]ijk') == 'fgh'
assert alphabet_war('ab#de[fgh]ij#k') == ''
assert alphabet_war('##abde[fgh]ijk') == ''
assert alphabet_war('##abde[fgh]') == ''
assert alphabet_war('##abcde[fgh]') == ''
assert alphabet_war('abcde[fgh]') == 'abcdefgh'    
assert alphabet_war('##abde[fgh]ijk[mn]op') == 'mn'
assert alphabet_war('#abde[fgh]i#jk[mn]op') == 'mn'
assert alphabet_war('[ab]adfd[dd]##[abe]dedf[ijk]d#d[h]#') == 'abijk'