# https://www.codewars.com/kata/51e056fe544cf36c410000fb/train/python

import re

def top_3_words(text):
    text = re.sub(r"(?:[^a-z'])+", " ", text.lower())
    words = text.split(" ")
    words = [w for w in words if re.search(r"[a-z]", w)]
    words = [re.search(r"([a-z']+)", w)[1] for w in words if re.search(r"([a-z']+)", w)]

    freq = {}

    for word in words:
        freq[word] = 1 if word not in freq else freq[word] + 1
    
    return sorted(freq, key=freq.get, reverse=True)[0:3]

assert top_3_words("a a a  b  c c  d d d d  e e e e e") == ["e", "d", "a"]
assert top_3_words("e e e e DDD ddd DdD: ddd ddd aa aA Aa, bb cc cC e e e") == ["e", "ddd", "aa"]
assert top_3_words("  //wont won't won't ") == ["won't", "wont"]
assert top_3_words("  , e   .. ") == ["e"]
assert top_3_words("  ...  ") == []
assert top_3_words("  '  ") == []
assert top_3_words("  '''  ") == []
assert top_3_words("""In a village of La Mancha, the name of which I have no desire to call to
mind, there lived not long since one of those gentlemen that keep a lance
in the lance-rack, an old buckler, a lean hack, and a greyhound for
coursing. An olla of rather more beef than mutton, a salad on most
nights, scraps on Saturdays, lentils on Fridays, and a pigeon or so extra
on Sundays, made away with three-quarters of his income.""") == ["a", "of", "on"]
assert top_3_words("TKRffuG-,,??GehowJuuA,-_GehowJuuA!nPJW?GehowJuuA:.GehowJuuA_!_;GehowJuuA?..GehowJuuA:GehowJuuA:-/TKRffuG:?GehowJuuA?:nPJW_:!:-ShyY. -_:GehowJuuA//nPJW,GehowJuuA-;-:ShyY?-:GehowJuuA!!.;:GehowJuuA!,/;GehowJuuA:!-GehowJuuA/?/TKRffuG ;!;GehowJuuA:/GehowJuuA/ !//GehowJuuA!::;?GehowJuuA!.:/nPJW:;//?nPJW,:!;TKRffuG,-.,GehowJuuA.../;ShyY/:_ShyY!GehowJuuA.!TKRffuG,.GehowJuuA,_;GehowJuuA,__ ?GehowJuuA_GehowJuuA !!? GehowJuuA:,nPJW?!,.TKRffuG-.nPJW_.GehowJuuA?_/GehowJuuA TKRffuG.?:GehowJuuA,? nPJW.;:GehowJuuA/-//") == ['gehowjuua', 'npjw', 'tkrffug']